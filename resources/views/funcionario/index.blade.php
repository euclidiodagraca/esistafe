@extends('layouts.app')
@section('content')

<h3 style="margin-left: 5% !important; margin-bottom: 2% !important;" class="h4">Lista de funcionarios registados</h3>
<div style="margin-right: 5% !important; margin-left: 5% !important">

<table id="example" class="table table-striped table-bordered mx-auto">
    <thead class="thead-dark">
        <tr>
            <th scope="col">Nome</th>
            <th scope="col">Apelido</th>
            <th scope="col">Número de BI</th>
            <th scope="col">Nuit</th>
            <th scope="col">Data de nascimento</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($funcionarios as $funcionario)
        <tr>
            <td>{{ $funcionario->nome }}</td>
            <td>{{ $funcionario->apelido }}</td>
            <td>{{ $funcionario->bi }}</td>
            <td>{{ $funcionario->nuit }}</td>
            <td>{{ date('d-m-Y', strtotime($funcionario->data_de_nascimento)) }}</td>
        </tr>
        @endforeach
    </tbody>

</table>

{!! $funcionarios->links() !!}
</div>
<script type="text/javascript" defer>
    $(document).ready(function () {
        $('#example').DataTable({
        responsive: true,
        "language": {
        searchPlaceholder: "Pesquisar ...",
        url: 'https://cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json'
        }
        });
        });
</script>
@endsection
