@extends('layouts.app')

@section('content')

<div class="container">

    @include('layouts.flash_messages')

    <div class="row no-gutters" style="margin-top:2%">
        <div class="col-sm-8 col-md-8 mycontent-left" style="border-right: 1px dashed; border-color: #CECECE">

            <form style="margin-top: 3%" method="POST" action="{{ route('funcionario.procurar') }}">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-8">
                        <h5 class="font-weight-bold " for="nome">Pesquisar pelo Número de Identificação Tributária</h5>

                    </div>
                </div>
                <div class="form-group col-md-10">
                    <input class="form-control" placeholder="Número de Identificação Tributária" name="nuit"
                        type="number" id="nuit">
                    {{-- <label class="text-monospace form-check-label" for="nuit">
                            Confirmo a minha prova de vida ao ano de {{ now()->year }}
                    </label> --}}
                    <br>
                    <button type="submit" class="btn btn-info text-white font-weight-bold col-md-4">Pesquisar</button>
                </div>

            </form>

        </div>

        @if (session('funcionario'))

        @if(session('funcionario') != "NULL" )
        <div class="col-8 col-md-4 mycontent-right" style="padding-left: 1%; margin-top: 5%">
            <div class="card border-danger mb-3">
                <div class="card-header text-success">Prova de vida em dia</div>
                <div class="card-body text-secondary">
                    <h5 class="card-title text-dark font-weight-bold">{{ session('funcionario')->nome}}</h5>
                    <p class="card-text">Número de BI: <strong>{{ session('funcionario')->bi }}</strong>
                    </p>
                </div>
            </div>


            <hr>
        @if(date('m', strtotime(session('funcionario')->data_de_nascimento)) > date('m'))
            <form style="margin-top: 3%" method="POST" action="{{ route('prova.store') }}">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-10">
                        <h5 class="font-weight-bold " for="nome">Confirmar a prova de vida</h5>

                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" onclick="isConfirmed()" name="is_confirmed" type="checkbox"
                            id="is_confirmed">
                        <label class="text-monospace text-small form-check-label" for="is_confirmed">
                            Confirmar a prova de vida ao ano de {{ now()->year }}
                        </label>
                    </div>
                </div>
                <button type="submit" disabled id="comfirm" class="btn btn-secondary">Confirmar
                    prova de
                    vida</button>
            </form>
            <div class="spinner-grow align-self-center" hidden style="width: 3rem; height: 3rem;" id="status" role="status">
                <span class="sr-only">Aguarde a confirmação...</span>
            </div>
            @else
            <div class="form-group col-md-12 ">
                <h6 class="font-weight-bold text-secondary">Aguarde pelo mês de
                    {{ date('m', strtotime(session('funcionario')->data_de_nascimento)) }} para realizar a prova de vida</h6>

            </div>
            @endif
        </div>

        @else
        <div class="col-8 col-md-4 mycontent-right" style="padding-left: 1%; margin-top: 5%">
            <div class="card border-danger mb-3">
                <div class="card-header text-danger">Funcionário não encontrado</div>
            </div>
        </div>

        @endif
        @endif
        {{-- @endisset  --}}
    </div>
</div>
@endsection
<script>
    var click = document.getElementById('comfirm');
    var status = document.getElementById('status');
    var checkbox = document.getElementById('is_confirmed');
    function isConfirmed(){

        var click = document.getElementById('comfirm');
            var status = document.getElementById('status');
            var checkbox = document.getElementById('is_confirmed');
            if (checkbox.checked == true){
                checkbox.value = true
                click.disabled=false
                click.className = "btn btn-primary";
            }else{
                checkbox.value = false
                click.disabled=true
                click.className = "btn btn-secondary";
            }
    }

    function confirm(){
        var click = document.getElementById('comfirm');
        var status = document.getElementById('status');
        click.disabled=true
        status.hidden = false

        var check = document.getElementById('is_confirmed');
        var isConfirmed = check.value;
        $.ajax({
        /* the route pointing to the post function */
        url: '/prova',
        method: 'POST',
        /* send the csrf-token and the input to the controller */
        type: "POST",
        data: {"_token": "{{ csrf_token() }}", is_confirmed: isConfirmed},
        dataType: 'text',
        /* remind that 'data' is the response of the AjaxController */
            success: function (response) {
                status.hidden = true
                console.log(response)
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                status.hidden = true
                console.log("erro  "+textStatus);
            }
        });
    }

    function visibleCell() {
        var telefone = document.getElementById('telefone');
        telefone.disabled = false;
    }
</script>
