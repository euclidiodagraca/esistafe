@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">

                <div class="font-weight-light badge badge-dark text-wrap" style="width: 30%;">
                    {{ Auth::user()->name }}
                </div>
                <div class="card-body">
                    <h3 class="font-weight-normal" style="margin-top: 2%">Registo de funcionário no sistema de notificacao para prova de vida
                    </h3>
                    <hr>
                    <form style="margin-top: 3%" method="POST" action="{{ route('funcionario.store') }}">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label class="font-weight-bold" for="nome">Primeiros nomes</label>
                                <input type="text" name="nome" class="form-control @error('nome') is-invalid @enderror"
                                    placeholder="Ex:. Beltrano Cicrano" value="{{ @old("nome") }}" id="nome">
                                @error('nome')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label class="font-weight-bold" for="apelido">Apelido</label>
                                <input type="text" class="form-control @error('apelido') is-invalid @enderror"
                                    name="apelido" placeholder="Ex:. Fulano" value="{{ @old("apelido") }}"
                                    id=" apelido">

                                @error('apelido')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label class="font-weight-bold" for="bi">Número de Bilhete de identidade</label>
                                <input type="text" class="form-control @error('bi') is-invalid
                                    @enderror" name="bi" id="bi" placeholder="Ex:. 01011111111111B"
                                    value="{{ @old("bi") }}" autocomplete="nope" style="text-transform:uppercase">
                                @error('bi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label class="font-weight-bold" for="nuit">Número de Identificação Tributária</label>
                                <input type="number" class="form-control @error('nuit') is-invalid
                                    @enderror" name="nuit" value="{{ @old("nuit") }}" id="nuit"
                                    placeholder="Ex:. 05050500">
                                @error('nuit')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label class="font-weight-bold" for="data_de_nascimento">Data de nascimento</label>
                                <input type="date" name="data_de_nascimento" value="{{ @old("data_de_nascimento") }}"
                                    class="form-control @error('data_de_nascimento')
                                    is-invalid @enderror" id="data_de_nascimento" placeholder="Ex:. 01/01/2020">
                                @error('data_de_nascimento')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror

                            </div>
                            <div class="form-group col-md-6">
                                <label class="font-weight-bold" for="telefone">Contacto telefónio</label>
                                <input type="number" class="form-control @error('telefone') is-invalid @enderror"
                                    name="telefone" id="telefone" value="{{ @old("telefone") }}"
                                    placeholder="Ex:. 840000000">
                                @error('telefone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold" for="password">Password</label>
                            <input type="password" autocomplete="new-password"
                                class="form-control @error('password') is-invalid @enderror" name="password"
                                id="password" placeholder="Senha">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" name="is_verdade" type="checkbox"
                                    value="{{ @old("is_verdade") }}" onclick="isVerdade()" id="is_verdade">
                                <label class="text-monospace form-check-label @error('is_verdade') is-invalid @enderror"
                                    for="is_verdade">
                                    Confirmo que a informação acima é verídica
                                </label>
                                @error('is_verdade')
                                <p class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </p>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary col-sm-4 font-weight-normal">Registar no sistema</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
    function isVerdade(){
        var checkbox = document.getElementById('is_verdade');
            if (checkbox.checked == true){
                checkbox.value = true
            }else{
                checkbox.value = null
            }
    }
</script>
