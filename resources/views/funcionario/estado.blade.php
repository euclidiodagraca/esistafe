@extends('layouts.app_funcionario')

@section('content')

<div class="container">

    @include('layouts.flash_messages')

    <div class="row no-gutters" style="margin-top:2%">
        <div class="col-sm-8 col-md-8 mycontent-left" style="border-right: 1px dashed; border-color: #CECECE">

            @if($mes < date('m'))
            <div class="form-group col-md-10">
                <h5 class="font-weight-bold text-secondary">Aguarde pelo mês de {{ date('m', strtotime(Auth::user()->data_de_nascimento)) }} para realizar a prova de vida</h5>

            </div>
            @elseif(!$isConfirmed and $mes == date('m'))
            <form style="margin-top: 3%" method="POST" action="{{ route('prova.store') }}">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <h4 class="font-weight-bold " for="nome">Confirmar a prova de vida</h4>

                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" onclick="isConfirmed()" name="is_confirmed" type="checkbox"
                            id="is_confirmed">
                        <label class="text-monospace form-check-label" for="is_confirmed">
                            Confirmo a minha prova de vida ao ano de {{ now()->year }}
                        </label>
                    </div>
                </div>
                <button type="submit" disabled id="comfirm" class="btn btn-secondary">Confirmar
                    prova de
                    vida</button>
            </form>
            @elseif(!$isConfirmed and $mes > date('m'))
            <div class="form-group col-md-12">
                <h5 class="font-weight-bold text-secondary">Não realizou a prova de vida no mês próprio</h5>

            </div>
            @endif
            <div class="spinner-grow align-self-center" hidden style="width: 3rem; height: 3rem;" id="status"
                role="status">
                <span class="sr-only">Aguarde a confirmação...</span>
            </div>

            {{-- Dados do funcionário  --}}

            <form style="margin-top: 10%; margin-right: 5%" method="POST"
                action="{{ route('funcionario.update', Auth::user()->id) }}">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <h5 class="font-weight-light text-secondary" for="nome">Seu número de telefone já não é o mesmo?
                        </h5>

                    </div>
                </div>
                <div class="form-group">
                    <label class="font-weight-bold" for="telefone">Contacto telefónio</label>
                <input disabled type="number" value="{{ substr(Auth::user()->telefone, 4, 13)  }}" class="form-control" name="telefone" id="telefone">
                </div>

                <div class="btn-group" role="group" aria-label="First group">
                    <button type="button" onclick="visibleCell()" class="btn btn-dark">Editar contacto</button>
                    <button type="button" class="btn btn-success">Actualizar contacto telefónico</button>
                </div>
            </form>

        </div>
        <div class="col-6 col-md-4 mycontent-right" style="padding-left: 1%; margin-top: 5%">

            <h5 class="font-weight-bold text-secondary" style="margin-bottom: 5%">Lista de provas de vida</h5>

            @if($mes <= date('m') and !$isConfirmed)
            <div class="card border-danger mb-3" style="max-width: 18rem;">
                <div class="card-header text-danger">Confirmada</div>
                <div class="card-body text-secondary">
                    <h5 class="card-title text-dark font-weight-bold">Prova de vida de {{ date('Y') }}</h5>
                    <p class="card-text">Precisa confirmar a prova de vida no mês de
                        {{ date('m', strtotime(Auth::user()->data_de_nascimento))  }} de {{ date('Y') }}</p>
                </div>
            </div>
            @endif

            @foreach ($provas as $prova)

            <div class="card border-danger mb-3" style="max-width: 18rem;">
                <div class="card-header text-danger">{{ $prova->is_confirmed? 'Confirmada' : 'Não confirmada'  }}</div>
                <div class="card-body text-secondary">
                    <h5 class="card-title text-dark font-weight-bold">Prova de vida de {{ $prova->ano }}</h5>
                    <p class="card-text">Registou a confirmação da sua prova de vida em
                        <span class="font-weight-bold">{{ date('d - m - yy', strtotime($prova->created_at)) }}</span> às
                        {{ date('H:i:s', strtotime($prova->created_at)) }}</p>
                </div>
            </div>

            @endforeach
        </div>
    </div>
</div>
@endsection
<script>
    var click = document.getElementById('comfirm');
    var status = document.getElementById('status');
    var checkbox = document.getElementById('is_confirmed');
    function isConfirmed(){

        var click = document.getElementById('comfirm');
            var status = document.getElementById('status');
            var checkbox = document.getElementById('is_confirmed');
            if (checkbox.checked == true){
                checkbox.value = true
                click.disabled=false
                click.className = "btn btn-primary";
            }else{
                checkbox.value = false
                click.disabled=true
                click.className = "btn btn-secondary";
            }
    }

    function confirm(){
        var click = document.getElementById('comfirm');
        var status = document.getElementById('status');
        click.disabled=true
        status.hidden = false

        var check = document.getElementById('is_confirmed');
        var isConfirmed = check.value;
        $.ajax({
        /* the route pointing to the post function */
        url: '/prova',
        method: 'POST',
        /* send the csrf-token and the input to the controller */
        type: "POST",
        data: {"_token": "{{ csrf_token() }}", is_confirmed: isConfirmed},
        dataType: 'text',
        /* remind that 'data' is the response of the AjaxController */
            success: function (response) {
                status.hidden = true
                console.log(response)
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                status.hidden = true
                console.log("erro  "+textStatus);
            }
        });
    }

    function visibleCell() {
        var telefone = document.getElementById('telefone');
        telefone.disabled = false;
    }
</script>
