@extends('layouts.app')
@section('content')

@guest
<div class="container">
<section class="h-100">
    <header class="container h-100">
        <div class="d-flex align-items-center justify-content-center h-100">
            <div class="d-flex flex-column" style="margin-top:8%">
                <h4 class="text font-weight-bold align-self-center p-2">Sistema de Notificação para prova de vida
                </h4>
                <hr>
            </div>
        </div>
    </header>
</section>
<div class="row justify-content-center">
    <div class="card col-md-8">
        <div class="card-body">
            <h4 class="font-weight-normal" style="margin-top: 2%">Inicie a sessão no elerta e-Sistafe</h4>
            <hr>
            @include('layouts.flash_messages')
            <form style="margin-top: 2%" method="POST" action="{{ route('usuario.login') }}">
                @csrf
                <div class="form-group">
                    <label class="font-weight-bold" for="nuit">Introduza o seu número de NUIT</label>
                    <input type="number" class="form-control @error('nuit')  is-invalid @enderror" name="nuit" id="nuit"
                        aria-describedby="nuit" value="{{ old('nuit', '') }}" autocomplete="off">
                    @error('nuit')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label class="font-weight-bold" for="password">Senha</label>
                    <input type="password"  class="form-control  @error('password') is-invalid @enderror" name="password" id="password" autocomplete="off">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group row ">
                    <div class="col-md-8 offset-md float-left">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                Manter-me conectado
                            </label>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary float-right">Acessar</button>
            </form>
        </div>
    </div>
</div>
<section class="h-100">
    <header class="container h-100">
        <div class="d-flex align-items-center justify-content-center h-100">
            <div class="d-flex flex-column" style="margin-top:8%"></div>
                <a type="submit" href="{{ route('login') }}" class="btn btn-light font-weight-bold float-right">Área administrativa   <i class="fa fa-arrow-right"></i></a>
            </div>
        </div>
    </header>
</section>

@else

<section class="h-100">
    <header class="container h-100">
        <div class="d-flex align-items-center justify-content-center h-100">
            <div class="d-flex flex-column" style="margin-top:8%">
                <h1 class="text font-weight-bold align-self-center p-2">Alerta e-sistafe</h1>
                <h3 class="text text-secondary font-weight-light align-self-center p-2">Seja bem vindo</h3>
            </div>
        </div>
    </header>
</section>
</div>
@endguest
@endsection
