@extends('layouts.app_funcionario')

@section('content')
@if(Auth::check())
@if (Auth::user()->isAdmin())
<div class="container" style="margin-top: 3%">
    <div class="row">
        <div class="col-11" style="margin-bottom: 2%">
            <img src="storage/Emblem_of_Mozambique.svg.png" width="50" height="50" />

        </div>
        <div class="col-1">
            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                @csrf


                <button class="btn btn-link" style="color: red; margin-top: 5%" data-toggle="tooltip" data-placement="top"
                    title="Sair do sistema"><i class="fas fa-power-off"></i></button>
            </form>

        </div>
    </div>
    <div class="row d-flex align-items-center justify-content-center h-100">
        <div class="col-6">

            <div class="row">
                <div class="col">
                    <h4 class="font-weight-lighter text-muted">Bem vindo {{ Auth::user()->name }}</h4>
                    <hr>
                    <h2 class="font-weight-bold">Está conectado ao sistema de notificação para prova de vida</h2>
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p style="font-size:1.1rem; font-weight:100; line-height:2rem; color:#3e3e3e; margin-bottom: 16px">O
                        e-Sistafe é um sistema de alerta que permite os funcionários publicos receberem notificações
                        para que possam realizar a prova de vida !</p>
                <div class="form-row">
                    <a href="{{ route('funcionario.create') }}" type="button"
                        class="btn btn-dark align-self-center text-white col-md-5 font-weight-bold" style="font-size:1rem; padding:12px 0">
                        Registar funcionário
                    </a>

                    <hr>

                    <a href="{{ route('estado.formulario') }}" type="button"
                        class="btn btn-info align-self-center text-white col-md-5 font-weight-bold" style="font-size:1rem; padding:12px 0">
                        Realizar prova de vida
                    </a>
                </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="text-center">
                <img src="storage/alert.svg" height="350" width="400" class="rounded" alt="...">
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div style="border-bottom: 1px solid #E3E3E2; margin: 2%"></div>
    <p style="color: #666666; margin: 2%">© e-Sistafe {{ date('Y') }}</p>
</div>
@endif
@endif
@endsection
