@extends('layouts.app_funcionario')

@section('content')
<div class="container" style="margin-top: 3%">
   <div class="row">
       <div class="col-11" style="margin-bottom: 2%">
            <img src="storage/Emblem_of_Mozambique.svg.png" width="50" height="50" />

        </div>
        <div class="col-1">
            <a style="color: red" href="{{ route('sair') }}"
                style="margin-top: 5%" data-toggle="tooltip" data-placement="top" title="Sair do sistema">
                <i class="fas fa-power-off"></i>
            </a>
        </div>
   </div>
<div class="row d-flex align-items-center justify-content-center h-100">
    <div class="col-6">

        <div class="row">
            <div class="col">
                <h2 class="font-weight-bold">Está conectado ao sistema de notificação para prova de vida</h2>
            </div>
        </div>
        <div class="row" style="margin-top: 5rem">
            <div class="col">
                <p style="font-size:1.1rem; font-weight:100; line-height:2rem; color:#3e3e3e; margin-bottom: 16px">Este sistema de alerta permite os funcionários publicos receberem notificações para que possam realizar a prova de vida !</p>
                <a href="{{ route('usuario.estado') }}" type="button" class="btn btn-dark align-self-center text-white col-md-6 font-weight-bold" style="font-size:1rem; padding:12px 0">
                    Consultar sobre o meu estado
                </a>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="text-center">
            <img src="storage/alert.svg" height="350" width="400" class="rounded" alt="...">
        </div>
    </div>
  </div>
</div>

<div class="container">
    <div style="border-bottom: 1px solid #E3E3E2; margin: 2%"></div>
    <p style="color: #666666; margin: 2%">©  sistema de notificação de prova de vida {{  date('Y') }}</p>
</div>
@endsection
