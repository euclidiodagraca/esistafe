@extends('layouts.app')

@section('content')
<div class="container">
    <section class="h-100">
        <header class="container h-100">
            <div class="d-flex align-items-center justify-content-center h-100">
                <div class="d-flex flex-column" style="margin-top:8%">
                    <h2 class="text font-weight-bold align-self-center p-2"> Sistema de Notificação para prova de vida</h2>
                </div>
            </div>
        </header>
    </section>
    <hr class="col-md-4">
    <div class="row justify-content-center">

        <div class="card col-md-8">
            <div class="card-body">
                <h4 class="font-weight-light" style="margin-top: 2%">Área administrativa  </h4>
                <hr>
                <form style="margin-top: 2%" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <label class="font-weight-bold" for="nuit">Introduza o seu email</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                            id="email" aria-describedby="email" autocomplete="off">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold" for="password">Senha</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror"
                            name="password" id="password" autocomplete="off">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group row ">
                        <div class="col-md-8 offset-md float-left">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                    {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    Manter-me conectado
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                           <button type="submit" class="btn btn-primary float-right">Acessar</button>

                            @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Esqueceu a senha?') }}
                            </a>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
