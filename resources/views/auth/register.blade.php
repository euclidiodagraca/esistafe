@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <h3 class="font-weight-bold align-self-center p-2" style="margin-top: 6%">Área administrativa e-Sistafe</h3>
            <hr>
            <div class="card">

                <div class="card-body" style="margin-right: 5%; margin-left: 5%; margin-top: 2%">
                    <form method="POST" action="{{ route('register') }}" >
                        @csrf
                        <div class="form-group">
                            <label class="font-weight-bold" for="name">Introduza nome</label>
                            <input type="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"
                                id="name" aria-describedby="name" autocomplete="off">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label class="font-weight-bold" for="email">Endereço de email</label>

                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                        </div>

                        <div class="form-group">
                            <label class="font-weight-bold" for="password">Senha</label>

                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                        </div>

                        <div class="form-group">
                            <label class="font-weight-bold">Confirme a senha</label>
                                <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password">
                        </div>

                        <div class="form-group  row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary float-right col-md-4">
                                    {{ __('Registar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
