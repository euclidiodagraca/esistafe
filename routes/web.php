<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();

Route::get('/', 'FuncionarioController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('admin');
Route::resource('funcionario', 'FuncionarioController');
Route::get('funcionarios/todos', 'FuncionarioController@listaTodos');

Route::group(['middleware' => 'auth:funcionario'], function () {
    Route::get('/meu-estado', 'FuncionarioController@estado')->name('usuario.estado');
    Route::get('/sair', 'FuncionarioController@logout')->name('sair');
    Route::resource('prova', 'ProvaVidaController');
});

Route::group(['middleware' => 'auth:web'], function () {
    Route::get('/estado', 'FuncionarioController@formularioEstado')->name('estado.formulario');
    Route::post('/estado', 'FuncionarioController@pesquisar')->name('funcionario.procurar');
});

Route::prefix('usuario')->group(function () {
    Route::get('/acessar', 'Auth\FuncionarioLoginController@showLoginForm')->name('usuario.acessar');
    Route::post('/login', 'Auth\FuncionarioLoginController@login')->name('usuario.login');

});
