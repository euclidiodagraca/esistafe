<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class SuperUsuario extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // $role = new Role();
        // $role->name = "admin";
        // $role->save();
        $role_admin = Role::where("name", "admin")->first();

        $admin = User::create(array(
            'name' => 'managerusuario',
            'password' => Hash::make('supermalate'),
            'email' => 'superuser@email.com',
        ));
        $admin->roles()->attach($role_admin);
    }
}
