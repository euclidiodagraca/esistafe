<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProvasVida extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $fillable = [
        'is_confirmed',
        'ano',
    ];
    protected $table = 'provas_vidas';


    public function fucionario()
    {
        return $this->belongsTo('App\Funcionario, foreign_key');

    }
}
