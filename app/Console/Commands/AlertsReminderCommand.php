<?php

namespace App\Console\Commands;

use App\Funcionario;
use App\Jobs\SendAlertMessage;
use Illuminate\Console\Command;
use Illuminate\Queue\SerializesModels;

class AlertsReminderCommand extends Command
{
    use SerializesModels;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alert:send-wish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enviar alerta para funcionarios no seu mês de aniversario.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = date('m');
        $listaDeFuncionariosDoMes = array();
        $funcionarios = Funcionario::all();

        if (!empty($funcionarios)) {
            foreach ($funcionarios as $funcionario) {
                if ($funcionario->prova() == null) {
                    $month = date('m', strtotime($funcionario->data_de_nascimento));
                    $isDoMes = ($month == $today);
                    if ($isDoMes) {
                        array_push($listaDeFuncionariosDoMes, $funcionario);
                    }
                }
            }
        }

        foreach ($listaDeFuncionariosDoMes as $doMes) {
            SendAlertMessage::dispatch($doMes);
        }
    }
}
