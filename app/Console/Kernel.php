<?php

namespace App\Console;

use App\Console\Commands\AlertsReminderCommand;
use App\Funcionario;
use App\Jobs\SendAlertMessage;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command(AlertsReminderCommand::class)->hourlyAt(10)
        //         ->weeklyOn(3, '16:50');
        $schedule->call(function () {

            $today = date('m');
            $listaDeFuncionariosDoMes = array();
            $funcionarios = Funcionario::all();

            if (!empty($funcionarios)) {
                foreach ($funcionarios as $funcionario) {
                    if ($funcionario->prova() == null) {
                        $month = date('m', strtotime($funcionario->data_de_nascimento));
                        $isDoMes = ($month == $today);
                        if ($isDoMes) {
                            array_push($listaDeFuncionariosDoMes, $funcionario);
                        }
                    }
                }
            }

            foreach ($listaDeFuncionariosDoMes as $doMes) {
                SendAlertMessage::dispatch($doMes);
            }

        })->everyMinute();

        // $schedule->command(AlertsReminderCommand::class)->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
