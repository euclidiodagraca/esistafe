<?php

namespace App\Jobs;

use App\Funcionario;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Twilio\Rest\Client;

class SendAlertMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $funcionario;
    private $nexmoClient;
    private $basicCredentials;

    //Twillio
    private $user;
    private $twilioClient;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Funcionario $funcionario)
    {
        $this->funcionario = $funcionario;
        $this->user = $funcionario;
        $sid = config('services.twilio.account_sid');
        $token = config('services.twilio.auth_token');
        $this->twilioClient = new Client($sid, $token);

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $sId = config('services.twilio.account_sid');
        // $token = config('services.twilio.auth_token');
        // $this->basic = new \Nexmo\Client\Credentials\Basic($sId, $token);
        // $this->nexmoClient = new \Nexmo\Client($this->basic);
        // $from = config('services.twilio.phone_number');
        $template = "precisa realizar a prova de vida neste mes";
        // $body = 'Sr(a). ' . $this->funcionario->nome . ', ' . $template . '!';
        // $message = $this->twilioClient->messages->create(
        //     $this->funcionario->telefone,
        //     [
        //         'from' => '+12057362444',
        //         'body' => $body,
        //     ]
        // );

        // $client->messages->create(
        //     // the number you'd like to send the message to
        //     '+15558675309',
        //     [
        //         // A Twilio phone number you purchased at twilio.com/console
        //         'from' => '+15017250604',
        //         // the body of the text message you'd like to send
        //         'body' => 'Hey Jenny! Good luck on the bar exam!',
        //     ]
        // );

        // try {
        //     $message = $this->nexmoClient->message()->send([
        //         'to' => 258845395142,
        //         'from' => 'e-Sistafe',
        //         'text' => $body,
        //     ]);
        //     $response = $message->getResponseData();

        //     if ($response['messages'][0]['status'] == 0) {
        //         echo "The message was sent successfully\n";
        //     } else {
        //         echo "The message failed with status: " . $response['messages'][0]['status'] . "\n";
        //     }
        // } catch (Exception $e) {
        //     echo "The message was not sent. Error: " . $e->getMessage() . "\n";
        // }


        $api = config('services.nexmo.api_key');
        $key_secret = config('services.nexmo.api_secret');
        $basic = new \Nexmo\Client\Credentials\Basic($api, $key_secret);

        $client = new \Nexmo\Client($basic);

        $response = $client->message()->send(
            [
                // 'type' => $message->type,
                'from' => 'SNPV',
                'text' => 'Sr(a). '.$this->funcionario->nome . ', ' . $template . '!',
                'to' => $this->funcionario->telefone,
            ]
        );

        $message = $response->current();
    }
}
