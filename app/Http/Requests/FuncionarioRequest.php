<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class FuncionarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'apelido' => 'required|string|max:20',
            'nome' => 'required|string|min:3|max:50',
            'data_de_nascimento' => 'required|date|before_or_equal:' . \Carbon\Carbon::now()->subYears(18)->format('Y-m-d'),
            'bi' => 'required|max:14|min:13',
            'nuit' => 'required|digits:9',
            'telefone' => 'required|integer|digits:9',
            'password' => 'required|max:100',
            'is_verdade' => 'required'
        ];
    }

    public function messages()
    {
        return [

            'apelido.required' => 'O :attribute deve ser preenchido',
            'nome.required' => 'O :attribute deve ser preenchido',
            'bi.required' => 'O campo de :attribute deve ser preenchido',
            'unique' => 'O(a) :attribute já foi registado ',
            'nuit.required' => 'O número de :attribute deve ser preenchido ',
            'telefone.required' => 'O :attribute deve ser preenchido',
            'data_de_nascimento.required' => 'A :attribute deve ser preenchida',
            'password.required' => 'Precisa preencher :attribute',
            'is_verdade.required' => 'Precisa confirmar que esta informação é válida',
            'data_de_nascimento.before' => 'Precisa ter 18+ anos de idade',
            'data_de_nascimento.before_or_equal' => 'Este funcionário é menor de idade',
            'nome.max' => 'O Nome excedeu o número limite de caracteres que são 50',
            'nuit.digits' => 'O :attribute é iválido',
            'telefone.digits' => 'O :attribute é iválido',
            // 'bi.digits_between' => 'O :attribute é inválido',
            'apelido.max' => 'O Apelido excedeu o número limite de caracteres que são 50',

        ];
    }

    public function attributes()
    {
        return [
            'apelido' => 'Apelido',
            'nome' => 'Nome',
            'telefone' => 'Telefone',
            'bi' => 'Bilhete de identidade',
            'nuit' => 'Número de identidade tributária',
            'data_de_nascimento' => 'Data de nascimento',
            'password' => 'Senha',
            'is_verdade' => 'Confirmação da informação',
        ];
    }

}
