<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FuncionarioLoginController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest:funcionario');
    }

    public function showLoginForm()
    {

        if (Auth::check()) {
            return redirect('/');
        }else{
            return view('welcome');
        }
    }

    public function login(Request $request)
    {
        $validator = Validator($request->all(), [
            'nuit' => 'required|numeric',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/')
            ->withErrors($validator)
            ->withInput();
        }

        $credentials = ['nuit' => $request->get('nuit'), 'password' => $request->get('password')];

        if (Auth::guard('funcionario')->attempt($credentials, $request->remember)) {
            $request->session()->put('user', Auth::guard('funcionario')->user());
            return redirect()->intended(route('usuario.estado'));
        } else {

            return redirect('/')
                ->with('erro','Acesso negado')
                ->withInput($request->only('email', 'remember'));
        }

    }
}
