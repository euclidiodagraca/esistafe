<?php

namespace App\Http\Controllers;

use App\ProvasVida;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProvaVidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (Auth::check() && Auth::guard('funcionario')->user()) {

            $current_year = (int) date('Y');
            $user_id = (int) Auth::user()->id;
            $isConfirmed = (bool) $request->input('is_confirmed');
            $provaVida = ProvasVida::where('funcionario_id', $user_id)->where('ano', $current_year)->get();
            // return count($provaVida) > 0;
            if (!count($provaVida) > 0) {
                if ($isConfirmed) {
                    $prova = new ProvasVida();
                    $prova->funcionario_id = $user_id;
                    $prova->is_confirmed = $isConfirmed;
                    $prova->ano = $current_year;
                    $prova->save();
                    // echo $isConfirmed;
                    $provaVida = ProvasVida::where('funcionario_id', $user_id)->where('ano', $current_year)->get();
                    return back()->with('/meu-estado')->with(['provas' => $provaVida, 'sucesso' => 'Actualizou a prova de vida ao ano de '.date('Y')]);

                } else {
                    return back()->with('/meu-estado')->with(['provas' => $provaVida, 'erro' => 'Precisa confirmar a prova de vida']);
                }
            } else {
                return back()->with('/meu-estado')->with(['provas' => $provaVida, 'aviso' => 'A prova de vida para o ano de '.date('Y').' já foi actualizada antes']);

            }
        } else {
            return view('home_funcionario');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProvaVida  $provaVida
     * @return \Illuminate\Http\Response
     */
    public function show(ProvasVida $provaVida)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProvaVida  $provaVida
     * @return \Illuminate\Http\Response
     */
    public function edit(ProvasVida $provaVida)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProvaVida  $provaVida
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProvasVida $provaVida)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProvaVida  $provaVida
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProvasVida $provaVida)
    {
        //
    }
}
