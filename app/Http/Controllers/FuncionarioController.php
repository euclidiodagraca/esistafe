<?php

namespace App\Http\Controllers;

use App\Funcionario;
use App\Http\Requests\FuncionarioRequest;
use App\ProvasVida;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Twilio\Rest\Client;

class FuncionarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::guard('funcionario')->user()) {
            return view('home_funcionario');
        } else if (Auth::check() && Auth::user()->isAdmin()) {
            return view('home');
        } else {
            return view('welcome');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        if (Auth::check()) {
            $request->user()->authorizeRoles(['admin']);
            return view("funcionario.create");
        } else {
            return redirect('/');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FuncionarioRequest $request)
    {
        $request->user()->authorizeRoles(['admin']);

        $funcionario = new Funcionario();
        $funcionario->nome = $request->nome;
        $funcionario->apelido = $request->apelido;
        $funcionario->data_de_nascimento = $request->data_de_nascimento;
        $funcionario->bi = $request->bi;
        $funcionario->nuit = $request->nuit;
        $funcionario->telefone = "+258" . $request->telefone;
        $funcionario->password = Hash::make($request->password);
        $funcionario->save();

        return redirect("funcionarios/todos");
    }

    public function formularioEstado()
    {
        return view('funcionario.marcar_prova');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Funcionario  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function show(Funcionario $funcionario)
    {
        //
    }

    public function pesquisar(Request $request)
    {
        $funcionario = Funcionario::where('nuit', $request->input('nuit'))->first();
        if ($funcionario != null) {
            return redirect('/estado')->with('funcionario', $funcionario);
        } else {
            return redirect('/estado')->with('funcionario', "NULL");
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Funcionario  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function edit(Funcionario $funcionario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Funcionario  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Funcionario $funcionario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Funcionario  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Funcionario $funcionario)
    {
        //
    }

    public function alterarEstado(Funcionario $funcionario)
    {
        //
    }

    public function logout()
    {
        Auth::guard('funcionario')->logout();
        return redirect('/');
    }

    public function estado()
    {
        if (Auth::guard('funcionario')->user()) {

            $isConfirmed = ProvasVida::where('funcionario_id', Auth::user()->id)
                ->where('ano', date('Y'))->where('is_confirmed', 1)->first();

            $mes = date('m', strtotime(Auth::user()->data_de_nascimento));

            return view('funcionario.estado')->with(['isConfirmed' => $isConfirmed, 'mes' => $mes, 'provas' => ProvasVida::where('funcionario_id', Auth::user()->id)->get()]);
        } else {
            return view('home');
        }

    }

    public function listaTodos()
    {
        // return Role::all();

        $today = date('m');
        $listaDeFuncionariosDoMes = array();
        $funcionarios = Funcionario::paginate(10);
        // return $funcionarios;

        if (!empty($funcionarios)) {
            foreach ($funcionarios as $funcionario) {
                $month = date('m', strtotime($funcionario->data_de_nascimento));
                $isDoMes = $month == $today;
                if ($isDoMes) {
                    array_push($listaDeFuncionariosDoMes, $funcionario);
                }
            }
        }
        return view('funcionario.index', compact('funcionarios'));

    }
}
