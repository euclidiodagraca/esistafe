<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Funcionario extends Authenticatable
{
    use Notifiable;

    protected $guard = 'funcionario';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $fillable = [
        'nome',
        'apelido',
        'data_de_nascimento',
        'bi',
        'nuit',
        'telefone',
        'password',
        'is_verdade'
    ];
    protected $table = 'funcionarios';


    protected $hidden = [
        'password', 'remmenber_token'
    ];

    public function isAdmin()
    {
        return false;
    }

    public function provas()
    {
        return $this->hasMany('App\ProvaVida');
    }

    public function prova(){
        return $this->hasOne(ProvasVida::class)->where('ano', date('Y'))->first();
    }
}
